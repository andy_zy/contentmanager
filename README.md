# USER MANUAL #


## Description ##

The bookmarklet extends a functionality of the default UI Content Manager tool.

Features added:

- Multi-items uploading

- Multi-items downloading

- Multi-items removing

- Replacing items in use


## Browser Compability ##

Chrome(All), FF(All), Safari(All), IE(9+)

## How it works ##

1.Go to the Content Manager page in the UI and run ContentManager bookmarklet.

![1.png](https://bitbucket.org/repo/xqzLoj/images/3613185000-1.png)

2.New options are available: 

- remove unused assets 

- remove selected assets

- download all assets

- download selected assets


![2.png](https://bitbucket.org/repo/xqzLoj/images/2785049729-2.png)

3.Multi-items uploading available via drag-n-drop feature. Select pictures to be uploaded, drag them toward pictures list area.

![3.png](https://bitbucket.org/repo/xqzLoj/images/2838867884-3.png)

4.Clarification message will accrue in a case of some picture(s) (name and extension are same) already exists.

![4.png](https://bitbucket.org/repo/xqzLoj/images/1375222323-4.png)

5.Uploading process will be shown. 'Reload page' link will be shown when all items are uploaded. Press it to finish.

![5_001.png](https://bitbucket.org/repo/xqzLoj/images/273308419-5_001.png)